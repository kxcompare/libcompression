#ifndef LZREADWRITETOOLS_H
#define LZREADWRITETOOLS_H

#include <iostream>
#include <fstream>
#include <filesystem>
#include <vector>
#include <string>
#include "LZTriplet.h"

int intFromBytes(std::istream& is);

void intToBytes(std::ostream& os, int value);

void readFileUncompressed(std::string& byte_string, const std::string &filename);

void createFileCompressed(std::vector<LZTriplet>& encoded, const std::string &filename);

void readFileCompressed(std::vector<LZTriplet>& encoded, const std::string &filename);

void createFileUncompressed(std::string& byteString, const std::string &filename);

size_t getFileSize(const std::string &filename);

#endif //ALGOTEST_LZREADWRITETOOLS_H
