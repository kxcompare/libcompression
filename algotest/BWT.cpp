#include <iostream>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <list>
#include <fstream>
#include <filesystem>
#include <unistd.h>

#include "include/BWT.h"

std::wstring BWT::transform(const std::wstring &source) {
    auto suffix_arr = computeSuffixArray(source);
    auto bwt_arr = findLastChar(source, suffix_arr);
    return bwt_arr;
}

std::vector<int> BWT::computeSuffixArray(const std::wstring &input_text) {
    size_t len_text = input_text.length();
    std::vector<Rotation> suff(len_text);

    std::cout << len_text << std::endl;

    for (int i = 0; i < len_text; i++) {
        suff[i].index = i;
        suff[i].suffix = input_text.c_str() + i;
    }

    std::sort(suff.begin(), suff.end(), [](const Rotation &x, const Rotation &y) {
        return wcscmp(x.suffix, y.suffix);
    });

    std::vector<int> suffix_arr(len_text);
    for (int i = 0; i < len_text; i++) {
        suffix_arr[i] = suff[i].index;
    }

    return suffix_arr;
}

std::wstring BWT::findLastChar(const std::wstring &input_text, const std::vector<int> &suffix_arr) {
    int len_text = input_text.length();
    std::vector<wchar_t> bwt_arr(len_text);
    for (int i = 0; i < len_text; i++) {
        int j = suffix_arr[i] - 1;
        if (j < 0) {
            j += len_text;
        }
        bwt_arr[i] = input_text[j];
    }
    std::wstringstream ss;
    for (wchar_t i : bwt_arr) {
        ss << i;
    }
    return ss.str();
}

std::wstring BWT::inverse(const std::wstring &bwt_arr) {
    std::wstringstream ss;
    int len_bwt = bwt_arr.length();

    int x = std::find(bwt_arr.begin(), bwt_arr.end(), L'$') - bwt_arr.begin();

    std::wstring sorted_bwt(bwt_arr.begin(), bwt_arr.end());
    std::sort(sorted_bwt.begin(), sorted_bwt.end());

    std::vector<std::list<int>> lists(4096);
    std::vector<int> l_shift(len_bwt);

    for (int i = 0; i < len_bwt; i++) {
        lists[bwt_arr[i]].push_back(i);
    }

    for (int i = 0; i < len_bwt; i++) {
        computeLShift(lists[sorted_bwt[i]], i, l_shift);
    }

    for (int i = 0; i < len_bwt; i++) {
        x = l_shift[x];
        ss << bwt_arr[x];
    }
    return ss.str();
}

void BWT::computeLShift(std::list<int> &list, int index, std::vector<int> &l_shift) {
    l_shift[index] = list.front();
    list.pop_front();
}

void BWT::compress(const std::string &filename_input, const std::string &filename_output) {
    std::wstring input = readWstringFromFile(filename_input, false);
    std::wstring transformed = transform(input);
    saveWstringToFile(transformed, BWT_FILENAME_OUT, false);
    RLE rle;
    rle.compress(BWT_FILENAME_OUT, filename_output);
}

void BWT::decompress(const std::string &filename_input, const std::string &filename_output) {
    RLE rle;
    rle.decompress(filename_input, BWT_FILENAME_IN);
    std::wstring input = readWstringFromFile(BWT_FILENAME_IN, true);
    std::wstring inverted = inverse(input);
    saveWstringToFile(inverted, filename_output, true);
}

std::wstring BWT::readWstringFromFile(const std::string &filename_input, bool is_decompressing = false) {
    if (!std::filesystem::exists(filename_input)) {
        throw std::runtime_error("no file exists!");
    }
    std::wifstream fin(filename_input);
    auto converter = new std::codecvt_utf8<wchar_t>;
    std::locale utf8_locale = std::locale(std::locale(), converter);
    fin.imbue(utf8_locale);
    wchar_t c;
    std::wstringstream wstringstream;
    while (fin >> std::noskipws >> c) {
        wstringstream << c;
    }
    if (!is_decompressing) {
        wstringstream << L'$';
    }
    fin.close();
    return wstringstream.str();
}

void BWT::saveWstringToFile(const std::wstring &data, const std::string &filename_output, bool is_decompressing = false) {
    std::wofstream out(filename_output);
    auto converter = new std::codecvt_utf8<wchar_t>;
    std::locale utf8_locale = std::locale(std::locale(), converter);
    out.imbue(utf8_locale);
    for (wchar_t i : data) {
        if (is_decompressing && i == '$') {
            continue;
        }
        out << i;
    }
    out.close();
}