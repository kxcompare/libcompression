#include "include/RLE.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <codecvt>

void RLE::compress(const std::string &filename_input, const std::string &filename_output) {
    std::wstring input = readWstringFromFile(filename_input);
    std::wstring compressed = encode(input);
    saveWstringToFile(compressed, filename_output);
}

void RLE::decompress(const std::string &filename_input, const std::string &filename_output) {
    std::wstring encoded = readWstringFromFile(filename_input);
    std::wstring uncompressed = decode(encoded);
    saveWstringToFile(uncompressed, filename_output);
}

std::wstring RLE::encode(const std::wstring &input) {
    if (input.length() == 1) {
        return input;
    }
    std::wstringstream stringstream;
    int sym_count = 1;
    for (int i = 1; i < input.length(); i++) {
        if (input[i] != input[i - 1]) {
            stringstream << input[i - 1] << (wchar_t)sym_count;
            sym_count = 1;
        } else {
            sym_count++;
        }
        if (i == input.length() - 1) {
            stringstream << input[i] << (wchar_t)sym_count;
        }
    }
    return stringstream.str();
}

std::wstring RLE::readWstringFromFile(const std::string &filename_input) {
    if (!std::filesystem::exists(filename_input)) {
        throw std::runtime_error("no file exists");
    }
    std::wifstream fin(filename_input);
    auto converter = new std::codecvt_utf8<wchar_t>;
    std::locale utf8_locale = std::locale(std::locale(), converter);
    fin.imbue(utf8_locale);
    wchar_t c;
    std::wstringstream wstringstream;
    while (fin >> std::noskipws >> c) {
        wstringstream << c;
    }
    fin.close();
    return wstringstream.str();
}

void RLE::saveWstringToFile(const std::wstring &data, const std::string &filename_output) {
    std::wofstream out(filename_output);
    auto converter = new std::codecvt_utf8<wchar_t>;
    std::locale utf8_locale = std::locale(std::locale(), converter);
    out.imbue(utf8_locale);
    for (wchar_t c : data) {
        out << c;
    }
    out.close();
}

std::wstring RLE::decode(const std::wstring &encoded) {
    std::wstringstream input_stream(encoded);
    std::wstringstream output_stream;
    wchar_t sym;
    wchar_t num;
    while (input_stream >> std::noskipws >> sym >> std::noskipws >> num) {
        int bit32 = (int)num;
        for (int i = 0; i < bit32; i++) {
            output_stream << sym;
        }
    }
    return output_stream.str();
}