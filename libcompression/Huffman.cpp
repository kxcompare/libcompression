#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <exception>

#include "include/Huffman.h"
#include "include/util/BitArray.h"

std::pair<std::string, std::map<char, int>> Huffman::encode(const std::string &text) {
    std::map<char, int> frequencies = getFrequencies(text);
    std::vector<HuffmanTreeNode *> treeNodes = getNodeList(frequencies);
    HuffmanTreeNode *tree = buildHuffmanTree(treeNodes);
    std::map<char, std::string> codes;
    for (auto  &[key, value] : frequencies) {
        codes[key] = tree->getCodeForCharacter(key, "");
    }
    std::stringstream stream;
    for (char i : text) {
        stream << codes[i];
    }
    return std::make_pair(stream.str(), frequencies);
}

std::map<char, int> Huffman::getFrequencies(const std::string &text) {
    std::map<char, int> result;
    for (char c : text) {
        result[c]++;
    }
    return result;
}

HuffmanTreeNode *Huffman::buildHuffmanTree(std::vector<HuffmanTreeNode *> &treeNodes) {
    while (treeNodes.size() > 1) {
        std::sort(treeNodes.begin(), treeNodes.end(),
                  [](HuffmanTreeNode *a, HuffmanTreeNode *b) -> bool {
                      return a->getWeight() > b->getWeight();
                  });
        HuffmanTreeNode *left = treeNodes.back();
        treeNodes.pop_back();
        HuffmanTreeNode *right = treeNodes.back();
        treeNodes.pop_back();

        auto *parent = new HuffmanTreeNode(0, left->getWeight() + right->getWeight(), left, right);
        treeNodes.push_back(parent);
    }
    return treeNodes.at(0);
}

std::string Huffman::decode(const std::string &encoded, HuffmanTreeNode *tree) {
    std::stringstream stream;
    HuffmanTreeNode *node = tree;
    for (char i : encoded) {
        node = i == '0' ? node->getLeft() : node->getRight();
        if (node->getContent() != 0) {
            stream << node->getContent();
            node = tree;
        }
    }
    return stream.str();
}

std::vector<HuffmanTreeNode *> Huffman::getNodeList(const std::map<char, int> &frequencies) {
    std::vector<HuffmanTreeNode *> treeNodes;
    for (auto &[key, value] : frequencies) {
        treeNodes.push_back(new HuffmanTreeNode(key, value));
    }
    return treeNodes;
}

void Huffman::writeToFile(const std::string &encoded, const std::map<char, int> &frequencies, const std::string &filename) {
    std::ofstream out(filename, std::ios::out | std::ios::binary);
    int freq_size = frequencies.size();
    out.write((const char *)&freq_size, sizeof(freq_size));
    for (auto &[k, v] : frequencies) {
        out.write((const char *)&k, sizeof(k));
        out.write((const char *)&v, sizeof(v));
    }
    int compressed_bits = encoded.length();
    BitArray bitArray(compressed_bits);
    for (int i = 0; i < encoded.length(); i++) {
        bitArray.set(i, encoded[i] != '0' ? 1 : 0);
    }
    out.write((const char *)&compressed_bits, sizeof(compressed_bits));
    for (int i = 0; i < bitArray.getSizeInBytes(); i++) {
        uint8_t data = (bitArray.getBytes()[i]);
        out.write((const char *)&data, sizeof(data));
    }
    out.close();
}

std::pair<std::string, std::map<char, int>> Huffman::readFromFile(const std::string &filename) {
    std::ifstream fin(filename, std::ios::in | std::ios::binary);
    if (!std::filesystem::exists(filename)) {
        throw std::runtime_error("no file exists!");
    }
    std::stringstream stringstream;
    int freq_size;
    fin.read((char *)&freq_size, sizeof(freq_size));
    std::map<char, int> frequencies;
    for (int i = 0; i < freq_size; i++) {
        char sym; int count;
        fin.read((char *)&sym, sizeof(sym));
        fin.read((char *)&count, sizeof(count));
        frequencies[sym] = count;
    }
    int bits_size;
    fin.read((char *)&bits_size, sizeof(bits_size));
    int byte_count = bits_size / 8 + (bits_size % 8 > 0 ? 1 : 0);
    auto *arr = new uint8_t[byte_count];
    BitArray bitArray(bits_size, arr);
    for (int i = 0; i < bitArray.getSizeInBytes(); i++) {
        fin.read((char *)&(arr[i]), sizeof(arr[i]));
    }
    for (int i = 0; i < bitArray.getSize(); i++) {
        stringstream << (bitArray.get(i) != 0 ? '1' : '0');
    }
    fin.close();
    return std::make_pair(stringstream.str(), frequencies);
}

void Huffman::decompress(const std::string &filename_input, const std::string &filename_output) {
    std::ofstream out(filename_output);
    auto[bits, frequencies] = readFromFile(filename_input);
    std::vector<HuffmanTreeNode *> treeNodes = getNodeList(frequencies);
    HuffmanTreeNode *tree = buildHuffmanTree(treeNodes);
    std::string decoded = decode(bits, tree);
    out << decoded;
    out.close();
}

void Huffman::compress(const std::string &input_filename, const std::string &output_filename) {
    std::ifstream fin(input_filename, std::ios::in);
    if (!std::filesystem::exists(input_filename)) {
        throw std::runtime_error("no file exists!");
    }
    std::stringstream ss;
    char c;
    while (fin >> std::noskipws >> c) {
        ss << c;
    }
    fin.close();
    auto[bits, freq] = encode(ss.str());
    writeToFile(bits, freq, output_filename);
}

std::string Huffman::decode(const std::string &encoded, const std::map<char, int> &frequencies) {
    std::vector<HuffmanTreeNode *> treeNodes = getNodeList(frequencies);
    HuffmanTreeNode *tree = buildHuffmanTree(treeNodes);
    return decode(encoded, tree);
}
