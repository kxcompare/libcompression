#ifndef ALGOTEST_LZTRIPLET_H
#define ALGOTEST_LZTRIPLET_H

struct LZTriplet
{
    int offset;
    int length;
    char next;

    LZTriplet() = default;
    LZTriplet(int offset, int length, char next) {
        this->offset = offset;
        this->length = length;
        this->next = next;
    };
    ~LZTriplet() = default;
};

#endif //ALGOTEST_LZTRIPLET_H
