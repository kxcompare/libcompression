#ifndef ALGOTEST_BITARRAY_H
#define ALGOTEST_BITARRAY_H

#include <cstdint>
#include <string>

class BitArray {

private:
    int size;
    int byte_count;
    uint8_t *bytes;
    uint8_t mask[8] = {0b00000001, 0b00000010, 0b00000100, 0b00001000,
                     0b00010000, 0b00100000, 0b01000000, 0b10000000};
public:
    explicit BitArray(int size);

    BitArray(int size, uint8_t *bytes);

    ~BitArray();

    int get(int index) const;

    void set(int index, int value);

    int getSize() const;

    int getSizeInBytes() const;

    uint8_t *getBytes() const;

    std::string tostring() const;
};


#endif //ALGOTEST_BITARRAY_H
