#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <map>
#include <string>
#include <vector>
#include <utility>

#include "util/HuffmanTreeNode.h"

class Huffman {

public:
    static void compress(const std::string &input_filename, const std::string &output_filename);

    static void decompress(const std::string &filename_input, const std::string &filename_output);

private:
    static std::map<char, int> getFrequencies(const std::string &text);

    static std::pair<std::string, std::map<char, int>> encode(const std::string &text);

    static std::string decode(const std::string &encoded, const std::map<char, int> &frequencies);

    static std::string decode(const std::string &encoded, HuffmanTreeNode *tree);

    static HuffmanTreeNode *buildHuffmanTree(std::vector<HuffmanTreeNode *> &treeNodes);

    static void writeToFile(const std::string &encoded, const std::map<char, int> &frequencies, const std::string &filename);

    static std::pair<std::string, std::map<char, int>> readFromFile(const std::string &filename);

    static std::vector<HuffmanTreeNode *> getNodeList(const std::map<char, int> &frequencies);
};

#endif
