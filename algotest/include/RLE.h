#ifndef ALGOTEST_RLE_H
#define ALGOTEST_RLE_H

#include <string>

class RLE {
public:
    RLE() = default;

    void compress(const std::string &filename_input, const std::string &filename_output);

    void decompress(const std::string &filename_input, const std::string &filename_output);

private:

    std::wstring encode(const std::wstring &input);

    std::wstring decode(const std::wstring &encoded);

    std::wstring readWstringFromFile(const std::string &filename_input);

    void saveWstringToFile(const std::wstring &data, const std::string &filename_output);
};


#endif //ALGOTEST_RLE_H
