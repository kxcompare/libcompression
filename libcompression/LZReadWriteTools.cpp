#include <cstring>
#include <filesystem>

#include "include/util/LZReadWriteTools.h"
#include "include/util/LZTriplet.h"

int intFromBytes(std::istream &is) {
    char bytes[4];
    for (char & byte : bytes) {
        is.get(byte);
    }
    int integer;
    std::memcpy(&integer, &bytes, 4);
    return integer;
}

void intToBytes(std::ostream &os, int value) {
    char bytes[4];
    std::memcpy(&bytes, &value, 4);
    os.write(bytes, 4);
}

void readFileUncompressed(std::string &byte_string, const std::string &filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    if (!std::filesystem::exists(filename)) {
        throw std::runtime_error("no file exists!");
    }
    byte_string = std::string(std::istreambuf_iterator<char>(file), {});
    file.close();
}

void createFileCompressed(std::vector<LZTriplet> &encoded, const std::string &filename) {
    std::ofstream out(filename, std::ios::out | std::ios::binary);
    for (auto triplet : encoded) {
        intToBytes(out, triplet.offset);
        out << triplet.next;
        intToBytes(out, triplet.length);
    }
    out.close();
}

void readFileCompressed(std::vector<LZTriplet> &encoded, const std::string &filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    if (!std::filesystem::exists(filename)) {
        throw std::runtime_error("no file exists!");
    }
    LZTriplet element{};
    while (file.peek() != std::ifstream::traits_type::eof()) {
        element.offset = intFromBytes(file);
        file.get(element.next);
        element.length = intFromBytes(file);
        encoded.push_back(element);
    }
    file.close();
}

void createFileUncompressed(std::string &byteString, const std::string &filename) {
    std::ofstream out(filename, std::ios::out | std::ios::binary);
    out << byteString;
    out.close();
}

size_t getFileSize(const std::string &filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    if (!std::filesystem::exists(filename)) {
        throw std::runtime_error("no file exists!");
    }
    file.seekg(0, std::ios::end);
    long long size = file.tellg();
    file.close();
    return size;
}