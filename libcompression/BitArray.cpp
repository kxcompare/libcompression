#include "include/util/BitArray.h"
#include <sstream>

BitArray::BitArray(int size) {
    this->size = size;
    byte_count = size / 8;
    if (size % 8 > 0) {
        byte_count++;
    }
    bytes = new uint8_t [byte_count];
}

BitArray::BitArray(int size, uint8_t *bytes) {
    this->size = size;
    this->bytes = bytes;
    this->byte_count = size / 8 + (size % 8 > 0 ? 1 : 0);
}

int BitArray::get(int index) const {
    int byte_index = index / 8;
    int bit_index = index % 8;
    return (bytes[byte_index] & mask[bit_index]) > 0 ? 1 : 0;
}

void BitArray::set(int index, int value) {
    int byte_index = index / 8;
    int bit_index = index % 8;
    if (value > 0) {
        bytes[byte_index] = bytes[byte_index] | mask[bit_index];
    } else {
        bytes[byte_index] = bytes[byte_index] & ~mask[bit_index];
    }
}

int BitArray::getSize() const {
    return size;
}

int BitArray::getSizeInBytes() const {
    return byte_count;
}

uint8_t *BitArray::getBytes() const {
    return bytes;
}

std::string BitArray::tostring() const {
    std::stringstream ss;
    for (int i = 0; i < size; i++) {
        ss << (get(i) > 0 ? '1' : '0');
    }
    return ss.str();
}

BitArray::~BitArray() {
    delete bytes;
}
