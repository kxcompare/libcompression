#ifndef ALGOTEST_BWT_H
#define ALGOTEST_BWT_H

#include <string>
#include <vector>
#include <list>

#include "RLE.h"

#define BWT_FILENAME_OUT "bwt_transform_out.bin"
#define BWT_FILENAME_IN "bwt_transform_in.bin"

struct Rotation {
    int index;
    const wchar_t *suffix;
};

class BWT {
public:

    BWT() = default;

    std::wstring transform(const std::wstring &source);

    std::wstring inverse(const std::wstring &bwt_arr);

    void compress(const std::string& filename_input, const std::string& filename_output);

    void decompress(const std::string& filename_input, const std::string& filename_output);

private:

    std::vector<int> computeSuffixArray(const std::wstring &input_text);

    std::wstring findLastChar(const std::wstring &input_text, const std::vector<int> &suffix_arr);

    void computeLShift(std::list<int> &list, int index, std::vector<int> &l_shift);

    std::wstring readWstringFromFile(const std::string &filename_input, bool is_decompressing);

    void saveWstringToFile(const std::wstring &data, const std::string &filename_output, bool is_decompressing);

};

#endif //ALGOTEST_BWT_H
