#ifndef LZ77_H
#define LZ77_H

#include <iterator>
#include <algorithm>
#include "util/LZTriplet.h"
#include "util/LZReadWriteTools.h"

struct slidingWindow {
    std::string lookahead_buffer;
    std::string history_buffer;

    size_t look_buffer_max;
    size_t hist_buffer_max;

    slidingWindow() = default;

    slidingWindow(std::string &history_buffer, std::string &lookahead_buffer) {
        this->history_buffer = history_buffer;
        this->lookahead_buffer = lookahead_buffer;
    }

    ~slidingWindow() = default;

    LZTriplet getLongestPrefix();
};

class LZ77 {

    slidingWindow window;

    std::string byteDataString;

    std::vector<LZTriplet> encoded;

    void encode();

    void decode();

    void reset();

public:

    void compress(const std::string &filename_input, const std::string &filename_output);

    void decompress(const std::string &filename_input, const std::string &filename_output);

    LZ77(size_t lookBufMaxSize, size_t histBufMaxSize);
};

#endif