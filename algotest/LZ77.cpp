#include "include/LZ77.h"
#include "include/util/LZTriplet.h"
#include "include/util/LZReadWriteTools.h"

LZTriplet slidingWindow::getLongestPrefix() {
    LZTriplet code(0, 0, lookahead_buffer[0]);
    size_t look_curr_len = lookahead_buffer.length() - 1;
    size_t hist_curr_len = history_buffer.length();
    for (size_t i = 1; i <= std::min(look_curr_len, hist_curr_len); i++) {
        std::string s = lookahead_buffer.substr(0, i);
        size_t pos = history_buffer.find(s);
        if (pos == std::string::npos) {
            break;
        }
        if ((history_buffer.compare(hist_curr_len - i, i, s) == 0)
            && (lookahead_buffer[0] == lookahead_buffer[i])) {
            pos = hist_curr_len - i;
        }
        size_t full_repeat = 0;
        if (hist_curr_len == pos + i) {
            while ((look_curr_len >= i + full_repeat + i)
                   && (lookahead_buffer.compare(i + full_repeat, i, s) == 0)) {
                full_repeat += i;
            }
            size_t part_repeat = i - 1;
            while (!((look_curr_len >= i + full_repeat + part_repeat)
                     && (lookahead_buffer.compare(i + full_repeat, part_repeat, s, 0, part_repeat) == 0)) && part_repeat) {
                part_repeat--;
            }
            full_repeat += part_repeat;
        }
        if (code.length <= i + full_repeat) {
            code = LZTriplet(hist_curr_len - pos, i + full_repeat, lookahead_buffer[i + full_repeat]);
        }
    }
    return code;
}

void LZ77::encode() {
    do {
        if ((window.lookahead_buffer.length() < window.look_buffer_max) && (byteDataString.length() != 0)) {
            int len = window.look_buffer_max - window.lookahead_buffer.length();
            window.lookahead_buffer.append(byteDataString, 0, len);
            byteDataString.erase(0, len);
        }
        LZTriplet triplet = window.getLongestPrefix();
        window.history_buffer.append(window.lookahead_buffer, 0, triplet.length + 1);
        window.lookahead_buffer.erase(0, triplet.length + 1);
        if (window.history_buffer.length() > window.hist_buffer_max) {
            window.history_buffer.erase(0, window.history_buffer.length() - window.hist_buffer_max);
        }
        encoded.push_back(triplet);
    } while (window.lookahead_buffer.length());
}

void LZ77::decode() {
    for (auto code : encoded) {
        int length = code.length;
        if (length) {
            std::string s = byteDataString.substr(byteDataString.length() - code.offset, std::min(length, code.offset));
            while (length) {
                int repeat = std::min(length, static_cast<int>(s.length()));
                byteDataString.append(s, 0, repeat);
                length -= repeat;
            }
        }
        byteDataString.append(1, code.next);
    }
}

void LZ77::reset() {
    encoded.clear();
    window.history_buffer.clear();
    window.lookahead_buffer.clear();
    byteDataString.clear();
}

void LZ77::compress(const std::string &filename_input, const std::string &filename_output) {
    readFileUncompressed(byteDataString, filename_input);
    encode();
    createFileCompressed(encoded, filename_output);
    reset();
}

void LZ77::decompress(const std::string &filename_input, const std::string &filename_output) {
    readFileCompressed(encoded, filename_input);
    decode();
    createFileUncompressed(byteDataString, filename_output);
    reset();
}

LZ77::LZ77(size_t lookBufMaxSize, size_t histBufMaxSize) {
    window.look_buffer_max = lookBufMaxSize * 1024;
    window.hist_buffer_max = histBufMaxSize * 1024;
}
