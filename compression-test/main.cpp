#include <iostream>
#include <filesystem>
#include <cstdint>
#include <chrono>

#include "include/Huffman.h"
#include "include/LZ77.h"
#include "include/RLE.h"
#include "include/BWT.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "no input file" << std::endl;
    }

    std::string filename = argv[1];
    std::filesystem::path orig_test_path = "/home/alexander/CLionProjects/compression-test/bin/" + filename;
    std::uintmax_t orig_size = std::filesystem::file_size(orig_test_path);

    {
        const std::string huffman_compressed_name = "huffman_compressed.bin";
        auto start_huffman_encoding = std::chrono::system_clock::now();
        Huffman::compress(filename, huffman_compressed_name);
        auto end_huffman_encoding = std::chrono::system_clock::now();

        std::chrono::duration<double> seconds = end_huffman_encoding - start_huffman_encoding;
        std::filesystem::path huffman_compressed_path = "/home/alexander/CLionProjects/compression-test/bin/" + huffman_compressed_name;
        std::uintmax_t huffman_size = std::filesystem::file_size(huffman_compressed_path);

        std::cout << "Коэффициент сжатия алгоритма Хаффмана = " << (double) orig_size / huffman_size << std::endl;
        std::cout << "Время сжатия алгоритма Хаффмана = " << seconds.count() << " с" << std::endl;

        auto start_huffman_decoding = std::chrono::system_clock::now();
        Huffman::decompress(huffman_compressed_name, "huffman_decompressed.txt");
        auto end_huffman_decoding = std::chrono::system_clock::now();
        seconds = end_huffman_decoding - start_huffman_decoding;

        std::cout << "Время распаковки алгоритма Хаффмана = " << seconds.count() << " с" << std::endl;
    }

    std::cout << std::endl;

    {
        const std::string lz_compressed_name = "lz_compressed.bin";
        auto start_lz_encoding = std::chrono::system_clock::now();
        LZ77 lz(10, 10);
        lz.compress(filename, lz_compressed_name);
        auto end_lz_encoding = std::chrono::system_clock::now();

        std::chrono::duration<double> seconds = end_lz_encoding - start_lz_encoding;
        std::filesystem::path lz_compressed_path = "/home/alexander/CLionProjects/compression-test/bin/" + lz_compressed_name;
        std::uintmax_t lz_size = std::filesystem::file_size(lz_compressed_path);

        std::cout << "Коэффициент сжатия алгоритма LZ77 = " << (double) orig_size / lz_size << std::endl;
        std::cout << "Время сжатия алгоритма LZ77 = " << seconds.count() << " с" << std::endl;

        auto start_lz_decoding = std::chrono::system_clock::now();
        lz.decompress(lz_compressed_name, "lz_decompressed.txt");
        auto end_lz_decoding = std::chrono::system_clock::now();
        seconds = end_lz_decoding - start_lz_decoding;

        std::cout << "Время распаковки алгоритма LZ = " << seconds.count() << " с" << std::endl;
    }

        std::cout << std::endl;

    {
        const std::string rle_compressed_name = "rle_compressed.bin";
        auto start_rle_encoding = std::chrono::system_clock::now();
        RLE rle;
        rle.compress(filename, rle_compressed_name);
        auto end_rle_encoding = std::chrono::system_clock::now();

        std::chrono::duration<double> seconds = end_rle_encoding - start_rle_encoding;
        std::filesystem::path rle_compressed_path = "/home/alexander/CLionProjects/compression-test/bin/" + rle_compressed_name;
        std::uintmax_t rle_size = std::filesystem::file_size(rle_compressed_path);

        std::cout << "Коэффициент сжатия алгоритма RLE = " << (double) orig_size / rle_size << std::endl;
        std::cout << "Время сжатия алгоритма RLE = " << seconds.count() << " с" << std::endl;

        auto start_rle_decoding = std::chrono::system_clock::now();
        rle.decompress(rle_compressed_name, "rle_decompressed.txt");
        auto end_rle_decoding = std::chrono::system_clock::now();
        seconds = end_rle_decoding - start_rle_decoding;

        std::cout << "Время распаковки алгоритма RLE = " << seconds.count() << " с" << std::endl;
    }

        std::cout << std::endl;


    if (orig_size < 65000) {
        const std::string bwt_compressed_name = "bwt_compressed.bin";
        auto start_bwt_encoding = std::chrono::system_clock::now();
        BWT bwt;
        bwt.compress(filename, bwt_compressed_name);
        auto end_bwt_encoding = std::chrono::system_clock::now();

        std::chrono::duration<double> seconds = end_bwt_encoding - start_bwt_encoding;
        std::filesystem::path bwt_compressed_path = "/home/alexander/CLionProjects/compression-test/bin/" + bwt_compressed_name;
        std::uintmax_t bwt_size = std::filesystem::file_size(bwt_compressed_path);

        std::cout << "Коэффициент сжатия алгоритма BWT+RLE = " << (double) orig_size / bwt_size << std::endl;
        std::cout << "Время сжатия алгоритма BWT+RLE = " << seconds.count() << " с" << std::endl;

        auto start_bwt_decoding = std::chrono::system_clock::now();
        bwt.decompress(bwt_compressed_name, "bwt_decompressed.txt");
        auto end_bwt_decoding = std::chrono::system_clock::now();
        seconds = end_bwt_decoding - start_bwt_decoding;

        std::cout << "Время распаковки алгоритма BWT+RLE = " << seconds.count() << " с" << std::endl;
    }
}
