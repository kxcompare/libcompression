#ifndef LZTRIPLET_H
#define LZTRIPLET_H

struct LZTriplet
{
    int offset;
    int length;
    char next;

    LZTriplet() = default;
    LZTriplet(int offset, int length, char next) {
        this->offset = offset;
        this->length = length;
        this->next = next;
    };
    ~LZTriplet() = default;
};

#endif
