#ifndef LIBCOMPRESSION_HUFFMANTREENODE_H
#define LIBCOMPRESSION_HUFFMANTREENODE_H

#include <string>

class HuffmanTreeNode {

private:
    int weight;
    char content;
    HuffmanTreeNode *left;
    HuffmanTreeNode *right;

public:
    HuffmanTreeNode(char c, int weight);

    HuffmanTreeNode(char content, int weight, HuffmanTreeNode *left, HuffmanTreeNode *right);

    std::string getCodeForCharacter(char c, std::string parent_path);

    int getWeight() const;

    char getContent() const;

    HuffmanTreeNode *getLeft() const;

    HuffmanTreeNode *getRight() const;
};


#endif //LIBCOMPRESSION_HUFFMANTREENODE_H
