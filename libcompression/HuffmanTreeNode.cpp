#include <string>
#include <iostream>

#include "include/util/HuffmanTreeNode.h"

HuffmanTreeNode::HuffmanTreeNode(char c, int weight) {
    this->content = c;
    this->weight = weight;
    this->left = nullptr;
    this->right = nullptr;
}

HuffmanTreeNode::HuffmanTreeNode(char content, int weight, HuffmanTreeNode *left, HuffmanTreeNode *right) {
    this->content = content;
    this->weight = weight;
    this->left = left;
    this->right = right;
}

std::string HuffmanTreeNode::getCodeForCharacter(char c, std::string parent_path) {
    if (content == c) {
        return parent_path;
    } else {
        if (left != nullptr) {
            std::string path = left->getCodeForCharacter(c, parent_path + '0');
            if (!path.empty()) {
                return path;
            }
        }
        if (right != nullptr) {
            std::string path = right->getCodeForCharacter(c, parent_path + '1');
            if (!path.empty()) {
                return path;
            }
        }
    }
    return "";
}

int HuffmanTreeNode::getWeight() const {
    return weight;
}

char HuffmanTreeNode::getContent() const {
    return content;
}

HuffmanTreeNode *HuffmanTreeNode::getLeft() const {
    return left;
}

HuffmanTreeNode *HuffmanTreeNode::getRight() const {
    return right;
}